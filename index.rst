Rayzeek is a motion sensor and occupancy sensor leading supplier. We offer all kinds of top-notch energy-saving lighting solutions from light strips, motion sensor switches, dimmers.

Wall mount and ceiling mount pir motion sensor switch with UL, FCC and RoHS certification. 

Our occupancy, vacancy and manual mode 3-in-1 occupancy sensors are widely installed and used in the US, UK and Europe markets.

Visit https://www.rayzeek.com